// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// певні операції виконуються паралельно з іншими операціями, без блокування виконання коду
// тобто  JS може виконувати багато різних завдань одночасно, таких як запити на сервер, обробка введення користувача,
// чекання відповіді від зовнішнього API і т.д  використовуються функції зворотного виклику (callback functions),
//  проміси (promises) і async/await


// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.



//знаходимо елементи HTML-сторінки: кнопка з селектором "button" і блок з селектором ".out".
const btn = document.querySelector("button");
const out = document.querySelector(".out");

//оголошуємо функцію sendRequest, яка приймає URL-адресу і виконує запит до цього URL за допомогою методу fetch()
//якщо запит успішний, то повертається відповідь у форматі JSON, якщо ні - повертається об'єкт помилки
const sendRequest = async (url) => {
  return await fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    return new Error("Something wrong!");
  });
};
// Оголошуються дві функції getIp і getDataIp, які використовують функцію sendRequest для отримання відповіді від сервера
//  Функція getIp виконує запит до сервісу ipify.org і повертає IP-адресу користувача у форматі JSON
//  Функція getDataIp виконує запит до сервісу ip-api.com з отриманою IP-адресою - повертає інформацію про цю адресу з 
//обраними полями в форматі JSON
// 1: country
// 2: countryCode
// 3: region
// 4: regionName
// 5: city
// 6: zip
// 7: lat
// 8: lon
// 9: timezone
// 10: isp
// 11: org
// 12: as
// 13: query

const getIp = () => sendRequest("https://api.ipify.org/?format=json");
const getDataIp = (ip) =>
  sendRequest(`http://ip-api.com/json/${ip}?fields=1589273`);

// Додаємо обробник події на клік по кнопці, який викликає функцію getIp, отримує IP-адресу і передає її в функцію getDataIp. 
// Результат запиту до getDataIp передається у функцію renderOut, 
// яка відображає отриману інформацію про IP-адресу на сторінці

btn.addEventListener("click", () => {
  getIp().then(({ ip }) => {
    getDataIp(ip).then((data) => renderOut(ip, data));
  });
});

const renderOut = (ip, { continent, country, city, regionName, district }) => {
  out.innerHTML = `
		<h2>Result by ip ${ip}:</h2>
		<div>Continent: ${continent}</div>
		<div>Country: ${country}</div>
		<div>City: ${city}</div>
		<div>Region name: ${regionName}</div>
		<div>District: ${district ? district : "sorry... API field returned empty"}</div>
	`;
};
